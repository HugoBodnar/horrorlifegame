package casir.lifegame.model.entity;

public class Cell {

    private boolean isAlive;

    public Cell() {
        this.isAlive = false;
    }

    public void kill() {
        this.isAlive = false;
    }

    public void setAlive() {
        this.isAlive = true;
    }

    public boolean isAlive() {
        return this.isAlive;
    }
}
