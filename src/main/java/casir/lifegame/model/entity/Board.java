package casir.lifegame.model.entity;

public class Board {

    public Cell[][] content;
    private int width;
    private int height;

    public Board(int w, int h){
    	this.width = w;
        this.height = h;
        this.content = new Cell[this.width][this.height];

        Cell[] line;
        for (int x=0; x<this.width; x++){
            line = new Cell[this.height];
            for (int y=0; y<this.height; y++) {
                line[y] = new Cell();
            }
            this.content[x] = line;
        }
    }

    public boolean isEquals(Board board2) {
        for (int x=0; x<this.width; x++){
            for (int y=0; y<this.height; y++) {
                if (this.content[x][y].isAlive() != board2.content[x][y].isAlive()){
                    return false;
                }
            }
        }
        return true;
    }

    public Board clone(){
        Board clone = new Board(this.width, this.height);
        for (int x=0; x<this.width; x++){
            for (int y=0; y<this.height; y++) {
                clone.content[x][y] = new Cell();
                if(this.content[x][y].isAlive())
                    clone.content[x][y].setAlive();
            }
        }
        return clone;
    }

    public int getNumberOfNeighbours(int x, int y) {
    	int neighbours = 0;
    	for (int v= -1; v <= 1; v++){
            for (int h = -1; h <= 1; h++){
                if (!(v== 0 && h==0)){
                    if (x+h >= 0 && x+h < this.width && y + v >= 0 && y+v < this.height){
                        boolean ok =  this.content[x+h][y+v].isAlive();
                        if (ok){
                            neighbours ++;
                        }
                    }
                }
            }
        }
    	return neighbours;
    }

    public int getWidth() {
    	return this.width;
    }

    public int getHeight() {
    	return this.height;
    }
}
