package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class LifeGame {

	private int width;
	private int height;
	private Board board;
	private Board nextBoard;

    public static void main(String[] args){
		LifeGame game = new LifeGame(50, 180);
    }

    public LifeGame(int w, int h){
    	this.width = w;
		this.height = h;
        this.board = new Board(this.width, this.height);
       	this.nextBoard = new Board(this.width, this.height);

		for(int i = 0; i < 18; i++) {
	        nextBoard.content[0][i*10+1].setAlive();
	        nextBoard.content[0][i*10+3].setAlive();
	        nextBoard.content[1][i*10+4].setAlive();
	        nextBoard.content[2][i*10+0].setAlive();
	        nextBoard.content[2][i*10+4].setAlive();
	        nextBoard.content[3][i*10+0].setAlive();
	        nextBoard.content[3][i*10+4].setAlive();
	        nextBoard.content[4][i*10+4].setAlive();
	        nextBoard.content[5][i*10+1].setAlive();
	        nextBoard.content[5][i*10+4].setAlive();
	        nextBoard.content[6][i*10+2].setAlive();
	        nextBoard.content[6][i*10+3].setAlive();
	        nextBoard.content[6][i*10+4].setAlive();
		}

        while(!nextBoard.isEquals(board))
            this.step();

        UserInterface.displayEndMessage();
    }

    public void step() {
		UserInterface.displayGameBoard(nextBoard);

		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

    	this.board =  nextBoard.clone();
        this.nextBoard = new Board(this.width, this.height);

        for (int x=0; x<this.width; x++){
            for (int y=0; y<this.height; y++) {

                int neighbours = this.board.getNumberOfNeighbours(x, y);

                if ( this.board.content[x][y].isAlive() ){
                    if (neighbours < 2 || neighbours > 3){
                    	this.nextBoard.content[x][y].kill();
                    } else {
                    	this.nextBoard.content[x][y].setAlive();
                    }
                } else {
                    if (neighbours == 3){
                    	this.nextBoard.content[x][y].setAlive();
                    } else {
                    	this.nextBoard.content[x][y].kill();
                    }
                }
            }
        }
    }
}
