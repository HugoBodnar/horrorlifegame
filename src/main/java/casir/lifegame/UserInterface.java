package casir.lifegame;

import casir.lifegame.model.entity.Board;

public class UserInterface{

    public static void displayGameBoard(Board board){
        System.out.print("\033[H\033[2J");
        System.out.flush();
        String toDisplay;
        for (int x=0; x<board.getWidth(); x++){
            toDisplay = "";
            for (int y=0; y<board.getHeight(); y++) {
                if (board.content[x][y].isAlive()){
                    toDisplay += "#";
                } else {
                    toDisplay += "_";
                }
            }
            System.out.println(toDisplay);
        }
    }

    public static void displayEndMessage() {
        System.out.println("Game over");
    }

}
